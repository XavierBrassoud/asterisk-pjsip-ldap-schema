# Asterisk PJSIP LDAP schema

New version of existing Asterisk LDAP schema that implement PJSIP classes.

## Import schemas
``` bash
sudo cp asterisk.ldap-schema /etc/ldap/schema/
sudo service slapd restart
sudo ldapadd -Y EXTERNAL -H ldapi:/// -f ./asterisk.ldif
```

## Setting up

See : [Asterisk : Setting up PJSIP with LDAP Realtime Driver][documentation].

[documentation]: https://medium.com/@xavier.brassoud/asterisk-setting-up-pjsip-with-ldap-realtime-driver-823a7471108a
